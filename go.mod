module gitea.com/tango/rbac

go 1.20

require (
	github.com/lunny/tango v0.5.6
	github.com/mikespook/gorbac v1.0.1
	github.com/tango-contrib/session v0.0.0-20170526074221-3115f8ddf72d
)

require (
	gitea.com/lunny/log v0.0.0-20190322053110-01b5df579c4e // indirect
	gopkg.in/mikespook/gorbac.v1 v1.0.1 // indirect
)
